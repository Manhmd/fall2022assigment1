"use strict";
/*add the event listener*/
document.addEventListener("DOMContentLoaded", setup);


/*function to setup*/
function setup(){
    const form = document.querySelector("form");
    const textarea = document.querySelector("textarea");

    form.addEventListener('change',generateTable());
    textarea.addEventListener('change', sizeTextarea(textarea));
}

/*function to generate table*/
function generateTable(){
    /* initiate all the variables*/
    var tblSection = document.getElementById("table_render_space");
    var rowCount = document.querySelector("#rowcount").value;
    var colCount = document.querySelector("#colcount").value;
    var tblWidth = document.querySelector("#tablewidth").value;
    var textColor = document.querySelector("#textcolor").value;
    var bgColor = document.querySelector("#backgroundcolor").value;
    var borderWidth = document.querySelector("#borderwidth").value;
    var borderColor = document.querySelector("#bordercolor").value;
    const textarea = document.querySelector("textarea");
    var table = document.createElement('table');
    var tbody = document.createElement('tbody');

    tblSection.innerHTML = "";
    table.appendChild(tbody);

    /*for loop to generate the table*/
    for (var i = 0; i < rowCount; i++){
        var tr = document.createElement('tr');

        tbody.appendChild(tr);

        for (var j = 0; j < colCount; j++){
            var td = document.createElement('td');

            td.appendChild(document.createTextNode(`Cell${i}${j}`));
            tr.appendChild(td);

            changeTextColor(td, textColor);
        }
    }
    /*change the features of the table by calling the functions from utilities*/
    changeBorderWidth(table, borderWidth);
    changeBorderColor(table, borderColor);
    changeBackground(table, bgColor);
    changeTagWidth(table, tblWidth);

    tblSection.appendChild(table);
    textarea.innerHTML = generateHTML(rowCount, colCount);
}

/*function to generate the html*/
function generateHTML(rowCount, colCount){
    var text = "<table>\n";

    for(var i = 0; i < rowCount; i++){
        text += "   <tr>\n";

        for(var j = 0; j < colCount; j++){
            text += `       <td>Cell${i}${j}</td>\n`;
        }
        text += "   </tr>\n";
    }
    text += "</table>";

    return text;
}

/* function to set the size of the text area */
function sizeTextarea(textarea){
    textarea.style.height = `${textarea.scrollHeight}px`;
}