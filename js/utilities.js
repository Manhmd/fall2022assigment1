"use strict"

function changeTextColor(elem, color) {
    elem.style.color = color;
}

function changeBackground(elem, color) {
    elem.style.backgroundColor = color;
}

function changeTagWidth(elem, width) {
    elem.witdh = `${width}%`;
}

function changeBorderColor(elem, color) {
    elem.style.borderColor = color;
}

function changeBorderWidth(elem, width) {
    elem.border = `${width}px`;
}